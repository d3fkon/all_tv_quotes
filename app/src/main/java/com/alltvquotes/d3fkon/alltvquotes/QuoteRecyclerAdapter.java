package com.alltvquotes.d3fkon.alltvquotes;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.List;

/**
 * Created by d3fkon on 30/3/17.
 */

class QuoteRecyclerAdapter extends RecyclerView.Adapter<QuoteRecyclerAdapter.QuoteViewHolder> {
    private List<String> quoteNameList, authorImgUrlList, quoteAuthorList;
    private String titleKey, seasonKey;
    private Context context;
    private Typeface RobotoLightItalic;

    interface QuoteOnClickListener {
        void onQuoteClick(int i);
    }

    private QuoteOnClickListener quoteOnClickListener = null;

    void registerQuoteListener(QuoteOnClickListener quoteOnClickListener) {
        this.quoteOnClickListener = quoteOnClickListener;
    }


    QuoteRecyclerAdapter(List<String> quoteNameList, List<String> authorImgUrlList, List<String> quoteAuthorList, String titleKey, String seasonKey, Context context) {
        this.quoteNameList = quoteNameList;
        this.authorImgUrlList = authorImgUrlList;
        this.quoteAuthorList = quoteAuthorList;
        this.titleKey = titleKey;
        this.seasonKey = seasonKey;
        this.context = context;
        RobotoLightItalic = Typeface.createFromAsset(context.getAssets(), "font/Roboto-LightItalic.ttf");
    }

    @Override
    public QuoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_quote, parent, false);
        return new QuoteViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final QuoteViewHolder holder, final int position) {
        YoYo.with(Techniques.FadeIn).duration(500).repeat(1).playOn(holder.cardView);
        holder.quoteText.setText(quoteNameList.get(position));
        holder.quoteText.setTypeface(RobotoLightItalic);
        holder.quoteAuthor.setText(quoteAuthorList.get(position));
        Glide.with(context).load(authorImgUrlList.get(position)).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return quoteNameList.size();
    }

    private void popUpOptionsMenu(final ImageButton imageButton, final int position) {
        PopupMenu popupMenu = new PopupMenu(imageButton.getContext(), imageButton);
        MenuInflater menuInflater = popupMenu.getMenuInflater();
        menuInflater.inflate(R.menu.menu_quote, popupMenu.getMenu());
        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(context, AuthorSpecificActivity.class);
                intent.putExtra("authorImgUrl", authorImgUrlList.get(position));
                intent.putExtra("authorName", quoteAuthorList.get(position));
                intent.putExtra("titleKey", titleKey);
                intent.putExtra("seasonKey", seasonKey);
                context.startActivity(intent);
                return false;
            }
        });
    }

    class QuoteViewHolder extends RecyclerView.ViewHolder {
        TextView quoteText, quoteAuthor;
        ImageView imageView;
        ImageButton button;
        CardView cardView;

        QuoteViewHolder(View itemView) {
            super(itemView);
            quoteText = (TextView) itemView.findViewById(R.id.quote_text);
            quoteText.setTypeface(Typeface.DEFAULT, 1);
            quoteAuthor = (TextView) itemView.findViewById(R.id.quote_author);
            imageView = (ImageView) itemView.findViewById(R.id.quote_image);
            button = (ImageButton) itemView.findViewById(R.id.quote_author_item);
            cardView = (CardView) itemView.findViewById(R.id.card_quote);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(v.getContext(), "Clicked", Toast.LENGTH_SHORT).show();
                    popUpOptionsMenu(button, getAdapterPosition());
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (v.getId() == button.getId()) {


                    } else quoteOnClickListener.onQuoteClick(getAdapterPosition());
                }
            });
        }
    }
}

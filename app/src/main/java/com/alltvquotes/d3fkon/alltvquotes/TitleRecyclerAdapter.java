package com.alltvquotes.d3fkon.alltvquotes;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.List;

/**
 * Created by d3fkon on 29/3/17.
 */

class TitleRecyclerAdapter extends RecyclerView.Adapter<TitleRecyclerAdapter.TitleViewHolder>{
    private List<String> titleNameList;
    private List<String> titleImgUrlList;
    private Context context;
    private Typeface RobotoBlack;

    TitleRecyclerAdapter(List<String> titleNameList, List<String> titleImgUrlList, Context context) {
        this.titleNameList = titleNameList;
        this.titleImgUrlList = titleImgUrlList;
        this.context = context;
        RobotoBlack = Typeface.createFromAsset(context.getAssets(), "font/Roboto-Black.ttf");
    }

    @Override
    public TitleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_title, parent, false);
        return new TitleViewHolder(v, context);
    }
    @Override
    public void onBindViewHolder(TitleViewHolder holder, int position) {
        YoYo.with(Techniques.FadeIn).duration(500).repeat(1).playOn(holder.cardView);
        holder.textView.setText(titleNameList.get(position));
        holder.textView.setTypeface(RobotoBlack);
        Glide.with(context).load(titleImgUrlList.get(position)).into(holder.imageView);
//        holder.cardView.setBackground.(DrawableUtils);
//        Glide.with(context).load(titleImgUrlList.get(position)).into(holder.cardView.getBackground())
    }

    @Override
    public void onViewAttachedToWindow(TitleViewHolder holder) {
        YoYo.with(Techniques.FadeIn).duration(500).repeat(1).playOn(holder.cardView);
        super.onViewAttachedToWindow(holder);
    }


    @Override
    public int getItemCount() {
        return titleNameList.size();
    }

    interface TitleOnClickListener {
        public void onTitleClicked(int i);
    }
    private TitleOnClickListener mlistener = null;
    void registerTitleListener(TitleOnClickListener listener) {
        this.mlistener = listener;
    }

    class TitleViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        ImageView imageView;
        TextView textView;
        int position;
        TitleViewHolder(View itemView, final Context context) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.card_title);
            imageView = (ImageView) itemView.findViewById(R.id.title_image);
            textView = (TextView) itemView.findViewById(R.id.title_name);
            position = getAdapterPosition();
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mlistener.onTitleClicked(getAdapterPosition());
                }
            });
        }
    }

}
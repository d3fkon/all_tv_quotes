package com.alltvquotes.d3fkon.alltvquotes;

import android.app.Application;
import android.content.Context;

import com.danikula.videocache.HttpProxyCacheServer;

/**
 * Created by d3fkon on 12/4/17.
 */
public class CacheService extends Application {

    private static HttpProxyCacheServer proxy;
    static Boolean isNull = true;

    public static HttpProxyCacheServer getProxy(Context context) {
//        CacheService app = (CacheService) context.getApplicationContext();
//        return proxy == null ? (proxy = newProxy(context)) : proxy;
        if (proxy != null)
            isNull = false;
        return isNull == true ? (proxy = newProxy(context)) : proxy;
    }

    private static HttpProxyCacheServer newProxy(Context context) {
        return new HttpProxyCacheServer(context);
    }
}

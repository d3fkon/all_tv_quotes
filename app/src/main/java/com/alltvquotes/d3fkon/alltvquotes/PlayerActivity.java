package com.alltvquotes.d3fkon.alltvquotes;


import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.danikula.videocache.HttpProxyCacheServer;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.IOException;

public class PlayerActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener {
    private MediaPlayer mediaPlayer;
    private boolean mediaIsPaused = true;
    TextView quoteTextView, quoteAuthorTextView;
    FloatingActionButton buttonPlayPause, buttonYtPlay;
    CollapsingToolbarLayout collapsingToolbarLayout;

    public PlayerActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        Snackbar networkSnackbar = Snackbar.make(findViewById(R.id.player_parent),
                getString(R.string.msg_snackbar), Snackbar.LENGTH_LONG);


        if (!isNetworkAvailable())
            networkSnackbar.show();
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        getSupportActionBar().hide();
        Bundle bundle = getIntent().getExtras();
        String titleName = bundle.getStringArrayList("finalNameList").get(0);
        String seasonName = bundle.getStringArrayList("finalNameList").get(1);
        String quoteText = bundle.getStringArrayList("finalNameList").get(2);
        String titleImgUrl = bundle.getStringArrayList("finalImgUrlList").get(0);
        String authorImgUrl = bundle.getStringArrayList("finalImgUrlList").get(1);
        String authorName = bundle.getString("finalAuthorName");
        String streamableUrl = bundle.getString("streamUrl");
        final String youtubeUrl = bundle.getString("youtubeUrl");

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        HttpProxyCacheServer proxy = CacheService.getProxy(PlayerActivity.this);
        collapsingToolbarLayout.setTitle(titleName);
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        ImageView titleImageView = (ImageView) findViewById(R.id.player_title_image);
        ImageView authorImageView = (ImageView) findViewById(R.id.player_author_image);
        quoteTextView = (TextView) findViewById(R.id.player_quote_text);
        quoteAuthorTextView = (TextView) findViewById(R.id.player_author);
        Glide.with(this).load(titleImgUrl).into(titleImageView);
        Glide.with(this).load(authorImgUrl).into(authorImageView);
        quoteTextView.setText(quoteText);
        quoteAuthorTextView.setText(seasonName + ", by " + authorName);

        buttonPlayPause = (FloatingActionButton) findViewById(R.id.player_playpause);
        buttonYtPlay = (FloatingActionButton) findViewById(R.id.player_yt_play);
        buttonPlayPause.setImageResource(R.drawable.ic_action_play);
        buttonYtPlay.setImageResource(R.drawable.ic_yt_play);
        buttonPlayPause.bringToFront();
        buttonYtPlay.bringToFront();

        final String proxyUrl = proxy.getProxyUrl(streamableUrl);
        buttonPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if (mediaIsPaused) {
                        mediaIsPaused = false;
                        buttonPlayPause.setImageResource(R.drawable.ic_action_pause);
                        try {
                            mediaPlayer.setDataSource(proxyUrl);
                            mediaPlayer.setOnPreparedListener(PlayerActivity.this);
                            mediaPlayer.prepareAsync();
                            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    buttonPlayPause.setImageResource(R.drawable.ic_action_play);
                                    mediaIsPaused = true;
                                    mp.reset();
                                }
                            });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    } else {
                        mediaIsPaused = true;
                        buttonPlayPause.setImageResource(R.drawable.ic_action_play);
                        mediaPlayer.reset();
                    }
            }
        });

        buttonYtPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PlayerActivity.this, YTPlayerActivity.class);
                intent.putExtra("youtubeUrl",youtubeUrl);
                startActivity(intent);
            }
        });

        //Ads
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }


    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onBackPressed() {
        if (!mediaIsPaused) {
            mediaPlayer.reset();
            finish();
        }
        else
            finish();
    }
}

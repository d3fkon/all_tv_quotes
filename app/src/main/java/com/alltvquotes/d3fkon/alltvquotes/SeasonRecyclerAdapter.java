package com.alltvquotes.d3fkon.alltvquotes;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.List;

/**
 * Created by d3fkon on 29/3/17.
 */

class SeasonRecyclerAdapter extends RecyclerView.Adapter<SeasonRecyclerAdapter.SeasonViewHolder> {
    private List<String> seasonList;
    private String titleImgUrl;
    private Context context;
    private Typeface RobotoBlack;

    SeasonRecyclerAdapter(List<String> seasonList, String titleImgUrl, Context context) {
        this.seasonList = seasonList;
        this.titleImgUrl = titleImgUrl;
        this.context = context;
        RobotoBlack = Typeface.createFromAsset(context.getAssets(), "font/Roboto-Black.ttf");
    }

    @Override
    public SeasonRecyclerAdapter.SeasonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.card_season, parent, false);
        return new SeasonViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SeasonRecyclerAdapter.SeasonViewHolder holder, int position) {
        YoYo.with(Techniques.FadeIn).duration(500).repeat(1).playOn(holder.cardView);
        holder.textView.setText(seasonList.get(position));
        holder.textView.setTypeface(RobotoBlack);
        Glide.with(context).load(titleImgUrl).into(holder.imageView);
    }

    @Override
    public void onViewAttachedToWindow(SeasonViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        YoYo.with(Techniques.FadeIn).duration(500).repeat(1).playOn(holder.cardView);

    }

    @Override
    public int getItemCount() {
        return seasonList.size();
    }


    interface SeasonOnClickListener {
        void onSeasonClick(int i);
    }

    private SeasonOnClickListener mseasonOnClickListener = null;

    void registerSeasonListener(SeasonOnClickListener seasonOnClickListener) {
        this.mseasonOnClickListener = seasonOnClickListener;
    }

    class SeasonViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cardView;
        TextView textView;
        ImageView imageView;
        int position;

        SeasonViewHolder(View itemView) {
            super(itemView);
            cardView = (LinearLayout) itemView.findViewById(R.id.card_season);
            textView = (TextView) itemView.findViewById(R.id.season_name);
            imageView = (ImageView) itemView.findViewById(R.id.season_image);
            position = getAdapterPosition();
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if (mseasonOnClickListener != null)
                    mseasonOnClickListener.onSeasonClick(getAdapterPosition());
                }
            });
        }
    }
}

package com.alltvquotes.d3fkon.alltvquotes;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AuthorSpecificActivity extends AppCompatActivity implements QueryQuoteRecyclerAdapter.OnAuthorSpecificQuoteClickListener{

    public String titleKey, seasonKey,
            authorImgUrl, titleImgUrl, authorName,
            titleName;
    public DatabaseReference databaseReference;
    public String databaseReferenceUrl = "https://alltvquotes.firebaseio.com/";
    List<String> quoteList, quoteIdList, quotePlayerUrlList, seasonNameList, youtubeStreamUrlList;
    ArrayList <String> finalNameList, finalIdList, finalImgUrlList;
    RecyclerView recyclerView;
    TextView textView;
    QueryQuoteRecyclerAdapter queryQuoteRecyclerAdapter;
    public int adapterPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_author_specific);
        Bundle bundle = getIntent().getExtras();
        titleKey = bundle.getString("titleKey");
        seasonKey = bundle.getString("seasonKey");
        authorName = bundle.getString("authorName");
        authorImgUrl = bundle.getString("authorImgUrl");
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setTitle("Fetching data");
        dialog.setMessage("Please Wait");
        dialog.setCancelable(false);
        dialog.show();
        textView = (TextView) findViewById(R.id.query_quote_head);
        textView.setText("All Quotes From\n" + authorName);
        // TODO: Fix titleName not loading

        databaseReference = FirebaseDatabase.getInstance().getReferenceFromUrl(databaseReferenceUrl);
        databaseReference.child(titleKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                titleName = dataSnapshot.child("TitleName").getValue(String.class);
                titleImgUrl = dataSnapshot.child("TitleImgUrl").getValue(String.class);

                dialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                dialog.dismiss();
            }
        });
        dialog.show();
        quoteList = new ArrayList<>();
        quoteIdList = new ArrayList<>();
        quotePlayerUrlList = new ArrayList<>();
        seasonNameList = new ArrayList<>();
        youtubeStreamUrlList = new ArrayList<>();
        queryQuoteRecyclerAdapter = new QueryQuoteRecyclerAdapter(quoteList, seasonNameList, this);
        recyclerView = (RecyclerView) findViewById(R.id.quote_author_specific_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(queryQuoteRecyclerAdapter);
        recyclerView.setNestedScrollingEnabled(false);

        dialog.show();
        databaseReference.child(titleKey).child("Seasons").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                quoteList.clear();
                quotePlayerUrlList.clear();
                quoteIdList.clear();
                seasonNameList.clear();
                youtubeStreamUrlList.clear();
                for (DataSnapshot __dS1 : dataSnapshot.getChildren()) {
                    for (DataSnapshot __dS2 : __dS1.child("Quotes").getChildren()) {
                        if (__dS2.child("Author").getValue(String.class).equals(authorName)) {
                            quoteList.add(__dS2.child("QuoteText").getValue(String.class));
                            quotePlayerUrlList.add(__dS2.child("QuotePlayerUrl").getValue(String.class));
                            quoteIdList.add(__dS2.child("QuoteId").getValue(String.class));
                            seasonNameList.add(__dS1.child("SeasonName").getValue(String.class));
                            youtubeStreamUrlList.add(__dS1.child("YoutubeUrl").getValue(String.class));
                            titleName = titleName + "";
                        }
                    }
                }
                queryQuoteRecyclerAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        queryQuoteRecyclerAdapter.registerListener(AuthorSpecificActivity.this);
    }

    @Override
    public void onAuthorSpecificQuoteClick(int i) {
        adapterPosition = i;
        finalNameList = new ArrayList<>();
        finalIdList = new ArrayList<>();
        finalImgUrlList = new ArrayList<>();

        finalNameList.add(titleName);
        finalNameList.add(seasonNameList.get(adapterPosition));
        finalNameList.add(quoteList.get(adapterPosition));
        finalIdList.add(quoteIdList.get(adapterPosition));

        finalImgUrlList.add(titleImgUrl);
        finalImgUrlList.add(authorImgUrl);

        Intent intent = new Intent(AuthorSpecificActivity.this, PlayerActivity.class);
        intent.putExtra("finalNameList", finalNameList);
        intent.putExtra("finalImgUrlList", finalImgUrlList);
        intent.putExtra("finalAuthorName", authorName);
        intent.putExtra("streamUrl",quotePlayerUrlList.get(adapterPosition));
        intent.putExtra("youtubeUrl", youtubeStreamUrlList.get(adapterPosition));

        startActivity(intent);
    }
}

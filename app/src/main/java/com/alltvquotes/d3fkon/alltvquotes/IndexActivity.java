package com.alltvquotes.d3fkon.alltvquotes;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class IndexActivity extends AppCompatActivity implements TitleRecyclerAdapter.TitleOnClickListener, SeasonRecyclerAdapter.SeasonOnClickListener, QuoteRecyclerAdapter.QuoteOnClickListener {

    public DatabaseReference databaseRootReference;
    public String databaseReferenceUrl = "https://alltvquotes.firebaseio.com/";
    public List<String> titleNameList;
    public List<String> titleImgUrlList;
    public List<String> titleKeylist;
    public List<String> seasonNameList;
    public List<String> seasonKeyList;
    public List<String> quoteNameList;
    public List<String> quoteAuthorImgUrlList;
    public List<String> quoteAuthorList;
    public List<String> quoteStreamUrlList;
    public List<String> youtubeStreamUrlList;
    public RecyclerView titleRecyclerView, seasonRecyclerView, quoteRecyclerView;
    private int mTitlePosition = 0, mSeasonPosition = 0, mQuotePosition = 0;
    private TextView titleHead, seasonHead, quoteHead;
    private ProgressDialog pD;
    public ArrayList<String> finalNameList;
    public ArrayList<String> finalImgUrlList;
    public int backCounter = 0;
    private Snackbar networkSnackbar;

    TitleRecyclerAdapter titleRecyclerAdapter;
    SeasonRecyclerAdapter seasonRecyclerAdapter;
    QuoteRecyclerAdapter quoteRecyclerAdapter;

    Typeface RobotoLightItalic;

    @Override
    public void onBackPressed() {
        if (backCounter == 1) {
            backCounter = 0;
            int pid = android.os.Process.myPid();
            android.os.Process.killProcess(pid);
        } else {
            Toast.makeText(IndexActivity.this, "Press back again to exit", Toast.LENGTH_SHORT).show();
            backCounter++;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_about: Intent intent = new Intent(IndexActivity.this, InfoActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        databaseRootReference = FirebaseDatabase.getInstance().getReferenceFromUrl(databaseReferenceUrl);
        titleHead = (TextView) findViewById(R.id.title_head);
        seasonHead = (TextView) findViewById(R.id.season_head);
        quoteHead = (TextView) findViewById(R.id.quote_head);
        titleRecyclerView = (RecyclerView) findViewById(R.id.title_recycler);
        seasonRecyclerView = (RecyclerView) findViewById(R.id.season_recycler);
        quoteRecyclerView = (RecyclerView) findViewById(R.id.quote_recycler);

        seasonHead.setVisibility(View.GONE);
        quoteHead.setVisibility(View.GONE);

        titleNameList = new ArrayList<>();
        titleImgUrlList = new ArrayList<>();
        titleKeylist = new ArrayList<>();
        seasonNameList = new ArrayList<>();

        networkSnackbar = Snackbar.make(findViewById(R.id.index_parent), getString(R.string.msg_snackbar), Snackbar.LENGTH_LONG);

        pD = new ProgressDialog(IndexActivity.this);
        pD.setTitle("Fetching data");
        pD.setMessage("Please Wait ...");
        pD.setCancelable(false);

        final Toast toast = Toast.makeText(this, "Database Error", Toast.LENGTH_SHORT);
        titleRecyclerAdapter = new TitleRecyclerAdapter(titleNameList, titleImgUrlList, this);
        titleRecyclerView.setAdapter(titleRecyclerAdapter);
        pD.show();
        databaseRootReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                pD.show();
                titleNameList.clear();
                titleImgUrlList.clear();
                titleKeylist.clear();
                for (DataSnapshot dS : dataSnapshot.getChildren()) {
                    titleNameList.add(dS.child("TitleName").getValue(String.class));
                    titleImgUrlList.add(dS.child("TitleImgUrl").getValue(String.class));
                    titleKeylist.add(dS.getKey());
                }
                if (titleNameList.size() != 0)
                    pD.dismiss();
                titleRecyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                toast.show();
            }
        });


        titleRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        titleRecyclerView.setHasFixedSize(true);
        titleRecyclerAdapter.registerTitleListener(this);
        titleHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titleRecyclerView.setAdapter(titleRecyclerAdapter);
            }
        });
        List<String> temp = new ArrayList<>();
        temp.add("SeasonList");
        seasonRecyclerAdapter = new SeasonRecyclerAdapter(temp
                , "http://www.dailystormer.com/wp-content/uploads/2015/09/BreakingBad.jpg", this);
        seasonRecyclerView.setHasFixedSize(true);
        seasonRecyclerAdapter.registerSeasonListener(this);
        seasonRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        seasonRecyclerView.setAdapter(seasonRecyclerAdapter);
        seasonRecyclerView.setVisibility(View.GONE);

        quoteRecyclerView.setHasFixedSize(true);
        quoteRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        quoteRecyclerView.setNestedScrollingEnabled(false);
        quoteRecyclerView.setVisibility(View.GONE);

        RobotoLightItalic = Typeface.createFromAsset(getAssets(), "font/Roboto-LightItalic.ttf");

    }

    @Override
    public void onTitleClicked(int i) {
        if (!isNetworkAvailable())
            networkSnackbar.show();
        mTitlePosition = i;
        seasonNameList = new ArrayList<>();
        seasonKeyList = new ArrayList<>();
        quoteRecyclerView.setVisibility(View.GONE);
        quoteHead.setVisibility(View.GONE);
        seasonRecyclerAdapter = new SeasonRecyclerAdapter(seasonNameList, titleImgUrlList.get(mTitlePosition), IndexActivity.this);
        seasonRecyclerView.setAdapter(seasonRecyclerAdapter);
        pD.show();
        databaseRootReference.child(titleKeylist.get(mTitlePosition)).child("Seasons")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        seasonRecyclerView.setVisibility(View.VISIBLE);
                        seasonNameList.clear();
                        seasonKeyList.clear();
                        for (DataSnapshot dS : dataSnapshot.getChildren()) {
                            seasonNameList.add(dS.child("SeasonName").getValue(String.class));
                            seasonKeyList.add(dS.getKey());
                        }
                        seasonRecyclerAdapter.notifyDataSetChanged();
                        pD.dismiss();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        seasonNameList = new ArrayList<>();
                        seasonKeyList = new ArrayList<>();
                        seasonRecyclerView.setVisibility(View.GONE);
                    }
                });

        pD.dismiss();
        if (seasonNameList != null) {
            seasonHead.setVisibility(View.VISIBLE);
            seasonRecyclerAdapter.registerSeasonListener(IndexActivity.this);
        } else
            Toast.makeText(IndexActivity.this, "Check your connection!", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onSeasonClick(int i) {
        if (!isNetworkAvailable())
           networkSnackbar.show();

        quoteNameList = new ArrayList<>();
        quoteAuthorImgUrlList = new ArrayList<>();
        quoteAuthorList = new ArrayList<>();
        quoteStreamUrlList = new ArrayList<>();
        youtubeStreamUrlList = new ArrayList<>();

        quoteRecyclerAdapter = new QuoteRecyclerAdapter(quoteNameList, quoteAuthorImgUrlList, quoteAuthorList, titleKeylist.get(mTitlePosition), seasonKeyList.get(mSeasonPosition), IndexActivity.this);
        quoteRecyclerView.setAdapter(quoteRecyclerAdapter);
        mSeasonPosition = i;

        pD.show();

        databaseRootReference.child(titleKeylist.get(mTitlePosition)).child("Seasons").child(seasonKeyList.get(mSeasonPosition)).child("Quotes")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        quoteAuthorList.clear();
                        quoteNameList.clear();
                        quoteAuthorImgUrlList.clear();
                        quoteStreamUrlList.clear();
                        youtubeStreamUrlList.clear();
                        quoteHead.setVisibility(View.VISIBLE);
                        for (DataSnapshot dS : dataSnapshot.getChildren()) {
                            quoteNameList.add(dS.child("QuoteText").getValue(String.class));
                            quoteAuthorImgUrlList.add(dS.child("AuthorImgUrl").getValue(String.class));
                            quoteAuthorList.add(dS.child("Author").getValue(String.class));
                            quoteStreamUrlList.add(dS.child("QuotePlayerUrl").getValue(String.class));
                            youtubeStreamUrlList.add(dS.child("YoutubeUrl").getValue(String.class));
                        }
                        quoteRecyclerAdapter.notifyDataSetChanged();
                        pD.dismiss();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        quoteHead.setVisibility(View.GONE);
                        quoteNameList = new ArrayList<>();
                        quoteAuthorImgUrlList = new ArrayList<>();
                        quoteAuthorList = new ArrayList<>();
                        quoteStreamUrlList = new ArrayList<>();

                    }
                });
        quoteRecyclerView.setVisibility(View.VISIBLE);
        quoteHead.setVisibility(View.VISIBLE);
        quoteRecyclerAdapter.registerQuoteListener(IndexActivity.this);
    }

    @Override
    public void onQuoteClick(int i) {
        if (!isNetworkAvailable()) {
            networkSnackbar.show();
        }
        mQuotePosition = i;
        finalNameList = new ArrayList<>();
        finalImgUrlList = new ArrayList<>();

        finalNameList.add(titleNameList.get(mTitlePosition));
        finalNameList.add(seasonNameList.get(mSeasonPosition));
        finalNameList.add(quoteNameList.get(mQuotePosition));


        finalImgUrlList.add(titleImgUrlList.get(mTitlePosition));
        finalImgUrlList.add(quoteAuthorImgUrlList.get(mQuotePosition));
        pD.show();
        Intent intent = new Intent(IndexActivity.this, PlayerActivity.class);
        intent.putExtra("finalNameList", finalNameList);
        intent.putExtra("finalImgUrlList", finalImgUrlList);
        intent.putExtra("finalAuthorName", quoteAuthorList.get(mQuotePosition));
        intent.putExtra("streamUrl", quoteStreamUrlList.get(mQuotePosition));
        intent.putExtra("youtubeUrl", youtubeStreamUrlList.get(mQuotePosition));
        startActivity(intent);
        pD.dismiss();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}

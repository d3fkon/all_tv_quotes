package com.alltvquotes.d3fkon.alltvquotes;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.List;

/**
 * Created by d3fkon on 8/4/17.

 */


class QueryQuoteRecyclerAdapter extends RecyclerView.Adapter<QueryQuoteRecyclerAdapter.QueryQuoteViewHolder> {

    interface OnAuthorSpecificQuoteClickListener {
        void onAuthorSpecificQuoteClick (int i);
    }

    private OnAuthorSpecificQuoteClickListener onAuthorSpecificQuoteClickListener;
    void registerListener (OnAuthorSpecificQuoteClickListener onAuthorSpecificQuoteClickListener) {
        this.onAuthorSpecificQuoteClickListener = onAuthorSpecificQuoteClickListener;
    }

    private List<String> queryQuoteList, seasonNameList;
    private Context context;
    private Typeface RobotoLightItalic;

    QueryQuoteRecyclerAdapter(List<String> queryQuoteList, List<String> seasonNameList, Context context) {
        this.queryQuoteList = queryQuoteList;
        this.seasonNameList = seasonNameList;
        this.context = context;
        RobotoLightItalic = Typeface.createFromAsset(context.getAssets(), "font/Roboto-LightItalic.ttf");

    }

    @Override
    public QueryQuoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_query_quote, parent, false);
        return new QueryQuoteViewHolder(v);
    }

    @Override
    public void onBindViewHolder(QueryQuoteViewHolder holder, int position) {
        YoYo.with(Techniques.FadeIn).duration(500).repeat(1).playOn(holder.cardView);
        holder.queryQuoteText.setText(queryQuoteList.get(position));
        holder.queryQuoteText.setTypeface(RobotoLightItalic);
        holder.queryQuoteSeason.setText(seasonNameList.get(position));
    }

    @Override
    public void onViewAttachedToWindow(QueryQuoteViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        YoYo.with(Techniques.FadeIn).duration(500).repeat(1).playOn(holder.cardView);
    }

    //NiggaHigga
    @Override
    public int getItemCount() {
        return queryQuoteList.size();
    }

    class QueryQuoteViewHolder extends RecyclerView.ViewHolder {
        TextView queryQuoteText, queryQuoteSeason;
        CardView cardView;
        QueryQuoteViewHolder(View itemView) {
            super(itemView);
            queryQuoteText = (TextView) itemView.findViewById(R.id.query_quote_text);
            queryQuoteSeason = (TextView) itemView.findViewById(R.id.query_quote_season);
            cardView = (CardView) itemView.findViewById(R.id.card_query_quote);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onAuthorSpecificQuoteClickListener.onAuthorSpecificQuoteClick(getAdapterPosition());
                }
            });
        }
    }
}
